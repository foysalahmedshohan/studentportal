<?php
include_once "adminheader.php";
include_once "adminsidebar.php";

if (isset($_GET['stid'])) {
        $stid = $_GET['stid'];
    }

?>

<div class="container-fluid addEmployee">

  <h2 style="color:#fff">Update Student Profile</h2>
  <a href="studentlist.php" class="btn btn-primary">Back to student list</a>

  <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $added = $st->updateStudent($_POST,$_FILES,$stid);
            if ($added) {
              echo "<h3 style='color:#C90000;text-align:center;margin:10px;'> $added </h3>";
            }
        }
    ?>
        <form action="" method="post" enctype="multipart/form-data">
            <?php
                $getstd = $st->getSingleStudent($stid); 
                $row = $getstd->fetch_assoc();
            ?>
            <div class="row">
                <div class="col-md-6  addEmployee">
                    <div class="form-group">
                      <label for="empName">Student Name:</label>
                      <input type="text" class="form-control addEmployee" id="empName" placeholder="Enter Student Name" name="name" value="<?php echo $row['name'];?>" required>
                    </div>
                    <div class="form-group">
                        <label for="bDate">Birth Date:</label>
                        <input type="date" class="form-control addEmployee" id="datepicker"  placeholder="Enter Student Birth Date" name="dob" value="<?php echo $row['bday'];?>" required>
                    </div>
                    <div class="form-group">
                        <label for="eBlodG">Blood Group:</label>
                        <input type="text" class="form-control addEmployee" id="sBlodG" placeholder="Enter Student Blood Group" name="bg" value="<?php echo $row['blood_group'];?>" required>
                    </div>
                       <div class="form-group">
                        <label for="employeeImage">Upload Photo</label>
                        <img src="<?php echo $row['image'];?>" style="width:220px; height:120px;" alt="">
                        <input type="file" class="form-control-file" id="sImage" name="image" aria-describedby="fileHelp">
                      </div>
                </div>

            <div class="col-md-6 addEmployee">
               
            <div class="form-group">
                <label for="deptName">Department:</label>
                <input type="text" class="form-control addEmployee" id="deptName" placeholder="Enter Department Name" name="dept" value="<?php echo $row['dept'];?>" required>
            </div>
            <div class="form-group">
                <label for="positionName">Address:</label>
                <input type="text" class="form-control addEmployee" id="address" placeholder="Enter Address" name="address" value="<?php echo $row['address'];?>" required>
            </div>
            <div class="form-group">
                <label for="eEmail">Email:</label>
                <input type="email" class="form-control addEmployee" id="sEmail" placeholder="Enter Student Mail" name="email" value="<?php echo $row['email'];?>" required>
            </div>
      	  
      	    <div class="form-group">
                <label for="eMobile">Mobile:</label>
                <input type="tel" class="form-control addEmployee" id="sMobile" placeholder="Enter Student Mobile" name="mobile" value="<?php echo $row['mobile'];?>" required>
            </div>
            <div class="form-group">
                <label for="joinDate">Admission Date:</label>
                <input type="date" class="form-control addEmployee" id="datepicker2"  placeholder="Enter Student Admission Date" name="joindate" value="<?php echo $row['admission_date'];?>" required>
            </div>

         
                  

            </div>
				
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                        <div class="form-inline">
                            <button type="submit" class="btn btn-success">Submit Profile</button>
                        </div>
                    </div>
                </div>
        </form>
</div>


<?php
include_once "adminfooter.php";
?>