<?php
include_once "adminheader.php";
include_once "adminsidebar.php";
?>

        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2  style="color:white;">DIU STUDENT PORTAL</h2>
                    <h5 style="color:red;">Welcome  </h5>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-envelope-o"></i>
                </span><a href="#" class="dashA">
                        <div class="text-box" >
                            <p class="main-text">Student Post</p>
                            <p class="text-muted">Student posts view</p>
                        </div></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-bars"></i>
                </span>
                        <a href="#" class="dashA">
                        <div class="text-box dashA" >
                            <p class="main-text">Create Notice</p>
                            <p class="text-muted">Notice for Student</p>
                        </div></a>
                    </div>
                </div>
               
            </div>
            <!-- /. ROW  -->
            <hr />
      </div>
<?php
include_once "adminfooter.php";
?>