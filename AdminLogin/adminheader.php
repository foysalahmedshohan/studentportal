<?php
ob_start();
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache"); 
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
  header("Cache-Control: max-age=2592000");
?>
<?php 
include "../libs/Session.php";
	Session::init();
  	Session::checkAdminSession();

$filepath = realpath(dirname(__FILE__));
  include_once ($filepath."/../libs/Database.php");
  include_once ($filepath."/../helpers/Format.php");

  spl_autoload_register(function($class){
    include_once "../classes/".$class.".php";
  });

  //creating object of classes
  $db = new Database();
  $fm = new Format();
  $ad = new Admin();
  $st = new Student();
?>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Student Portal</title>
    <link rel="icon" type="image/png" href="../style/image/mLogo.jpg" />
    <!-- BOOTSTRAP STYLES-->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../style/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <!-- CUSTOM STYLES-->
    <link href="../style/css/custom.css" rel="stylesheet" />
    <link href="../style/css/login.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <style>body{background-color: unset;
            background-image: none;}
            *{
              color: #fff;
            }
            </style>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">Admin</a>
        </div>
        <div style="color: white; padding: 15px 50px 5px 50px; float: right;font-size: 16px;">  
            <?php 
             if(isset($_GET['action']) && $_GET['action']=="logout"){
                          Session::destroy();
                          header("Location: adminLogin.php");
                      }
              ?>
            <a  class="btn btn-danger square-btn-adjust" href="?action=logout">Logout</a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->