<?php 
  include_once "adminheader.php";
  include_once "adminsidebar.php";

  if (isset($_GET['stid'])) {
        $stid = $_GET['stid'];
    }

 ?>

<div class="container-fluid">

  <h2>Add Course grade</h2>
    
    <?php 
      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $added = $ad->courseResult($_POST, $stid);
            if ($added) {
              echo "<h3 class='success' style='color:#fff;text-align:center;margin:10px;'>$added</h3>";
            }
        }
     ?>
        <form action="" method="post">
            <div class="row">
                  <div class="col-md-6  addEmployee">
          <div class="form-group">
        <label for="empName">Course Code:</label>
        <input type="text" class="form-control addEmployee" id="ccode" placeholder="Enter Course Code" name="course_code" required>
      </div>

                   
                   <div class="form-group">
      <label for="empName">Course Tittle:</label>
      <input type="text" class="form-control addEmployee" id="ctittle" placeholder="Enter Course Tittle" name="course_title" required>
    </div>
                    <div class="form-group">
                        <label for="eNid">Credit:</label>
                        <input type="text" class="form-control addEmployee" id="credit" placeholder="Enter Course Credit" name="credit" required>
                    </div>
                </div>

            <div class="col-md-6  addEmployee">

      <div class="form-group">
          <label for="eEmail">Grade:</label>
          <input type="text" class="form-control addEmployee" id="grade" placeholder="Enter Course Grade" name="grade" required>
      </div>
	  
	    <div class="form-group">
          <label for="eMobile">Grade Point:</label>
          <input type="text" class="form-control addEmployee" id="gpoint" placeholder="Enter Grade Point" name="gradepoint" required>
      </div>

            </div>
				
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                            <button type="submit" class="btn btn-success"">Submit</button>
                        </div>
                    </div>
                </div>
        </form>
</div>

<?php 
  include_once "adminfooter.php";
 ?>
