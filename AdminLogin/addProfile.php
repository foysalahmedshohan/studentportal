<?php
include_once "adminheader.php";
include_once "adminsidebar.php";
?>

<div class="container-fluid addEmployee">

  <h2 style="color:#fff">Add New Student</h2>
  
  <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $added = $ad->insertStudent($_POST,$_FILES);
            if ($added) {
              echo "<span class='success' style='color:#C90000;text-align:center;margin:10px;'>Student Profile Added successfully</span>";
            }
        }
    ?>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6  addEmployee">
                    <div class="form-group">
                      <label for="empName">Student Name:</label>
                      <input type="text" class="form-control addEmployee" id="empName" placeholder="Enter Student Name" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="eNid">ID Number:</label>
                        <input type="text" class="form-control addEmployee" id="sNid" placeholder="Enter Student ID No" name="st_id" required>
                    </div>

                    <div class="form-group">
                        <label for="eNid">Password:</label>
                        <input type="password" class="form-control addEmployee" id="sNid" placeholder="Enter Student Password" name="password" required>
                    </div>

                    <div class="form-group">
                        <label for="Egender">Gender:</label><br>
                        <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
                        <label class="radio-inline"><input type="radio" name="gender" value="Male">Male</label>
                        <label class="radio-inline"><input type="radio" name="gender" value="Others">Others</label>
                    </div>
                    <div class="form-group">
                        <label for="bDate">Birth Date:</label>
                        <input type="date" class="form-control addEmployee" id="datepicker"  placeholder="Enter Student Birth Date" name="dob" required>
                    </div>
                    <div class="form-group">
                        <label for="eBlodG">Blood Group:</label>
                        <input type="text" class="form-control addEmployee" id="sBlodG" placeholder="Enter Student Blood Group" name="bg" required>
                    </div>
                    
                </div>

            <div class="col-md-6 addEmployee">
               
            <div class="form-group">
                <label for="deptName">Department:</label>
                <input type="text" class="form-control addEmployee" id="deptName" placeholder="Enter Department Name" name="dept" required>
            </div>
            <div class="form-group">
                <label for="positionName">Address:</label>
                <input type="text" class="form-control addEmployee" id="address" placeholder="Enter Address" name="address" required>
            </div>
            <div class="form-group">
                <label for="eEmail">Email:</label>
                <input type="email" class="form-control addEmployee" id="sEmail" placeholder="Enter Student Mail" name="email" required>
            </div>
      	  
      	    <div class="form-group">
                <label for="eMobile">Mobile:</label>
                <input type="tel" class="form-control addEmployee" id="sMobile" placeholder="Enter Student Mobile" name="mobile" required>
            </div>
            <div class="form-group">
                <label for="joinDate">Admission Date:</label>
                <input type="date" class="form-control addEmployee" id="datepicker2"  placeholder="Enter Student Admission Date" name="joindate" required>
            </div>

            <div class="form-group">
                <label for="employeeImage">Upload Photo</label>
                <input type="file" class="form-control-file" id="sImage" name="image" aria-describedby="fileHelp" required>
            </div>
                  

            </div>
				
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                        <div class="form-inline">
                            <button type="submit" class="btn btn-success">Submit Profile</button>
                        </div>
                    </div>
                </div>
        </form>
</div>


<?php
include_once "adminfooter.php";
?>