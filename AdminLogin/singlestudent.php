<?php
include_once "adminheader.php";
include_once "adminsidebar.php";

?>

<div class="container-fluid">
 <h2 class='text-center'>Single student personal details</h2>
<a href="studentlist.php" class="btn btn-primary">Back to student list</a>
 <div class="row">
 	<ul class="list-group">
 	<?php 
		if (isset($_GET['stid'])) {
		        $stid = $_GET['stid'];
		        $getstd = $st->getSingleStudent($stid);
		       	$row = $getstd->fetch_assoc();
 	?>
    <li class="list-group-item list-group-item-success">Name: <?php echo $row['name'];?></li>
    <li class="list-group-item list-group-item-success">Student Id: <?php echo $row['st_id'];?></li>
    <li class="list-group-item list-group-item-success">Password: <?php echo $row['password'];?></li>
    <li class="list-group-item list-group-item-success">Date of birth: <?php echo $row['bday'];?></li>
    <li class="list-group-item list-group-item-success">Gender: <?php echo $row['gender'];?></li>
    <li class="list-group-item list-group-item-success">Blood Group: <?php echo $row['blood_group'];?></li>
    <li class="list-group-item list-group-item-success">Department: <?php echo $row['dept'];?></li>
    <li class="list-group-item list-group-item-success">Email: <?php echo $row['email'];?></li>
    <li class="list-group-item list-group-item-success">Mobile: <?php echo $row['mobile'];?></li>
    <li class="list-group-item list-group-item-success">Admission Date: <?php echo $row['admission_date'];?></li>
    <li class="list-group-item list-group-item-success">Address: <?php echo $row['address'];?></li>
    <li class="list-group-item list-group-item-success">Photo:<img style="width:250px; height: 150px;" src="<?php echo $row['image'];?>" alt="<?php echo $row['name'];?>"> </li>

    <?php
       	}
	?>
  </ul>
 </div>
</div>

<?php
include_once "adminfooter.php";
?>