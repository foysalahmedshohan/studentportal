<?php
include_once "adminheader.php";
include_once "adminsidebar.php";

if (isset($_GET['stid'])) {
        $stid = $_GET['stid'];
    }
?>

    <div class="container-fluid">
		<h2>Course Result</h2>    
				<div class="list-group" style="color:#000;">
		        <table class="table">
		          <?php
		            $getstudent = $st->getSingleStudent($stid);
		            $row = $getstudent->fetch_assoc();
		          ?>
		          <tr><td>Name:</td> <td><?php echo $row['name']; ?></td></tr>
		          <tr><td>ID:</td> <td><?php echo $row['st_id']; ?></td></tr>
		          <tr><td>Deparment: </td> <td><?php echo $row['dept']; ?></td></tr>
		        </table>
	      </div>  
			<hr>      
		  <table class="table table-bordered" id="MainTable">
		    <thead>
		      <tr>
		        <th>Sl</th>
		        <th>Course Code</th>
		        <th>Course Title</th>
		        <th>Credit</th>
		        <th>Grade</th>
		        <th>Grade Point</th>
		      </tr>
		    </thead>
		    <tbody>
		    <?php
				$getCourse = $st->getCourseResult($stid);
				if ($getCourse) {
					$i=0;
					while ($row = $getCourse->fetch_assoc()) {
						$i++;

			?>
		      <tr>
		        <td><?php echo $i;?></td>
		        <td><?php echo $row['course_code'];?></td>
		        <td><?php echo $row['course_title'];?></td>
		        <td><?php echo $row['credit'];?></td>
		        <td><?php echo $row['grade'];?></td>
		        <td><?php echo $row['gradepoint'];?></td>
		      </tr>
		      <?php
					}
				}else{
					echo "Not found any course !";
				}
		      ?>
		    </tbody>
		  </table>

<?php
include_once "adminfooter.php";
?>