<?php
include_once "adminheader.php";
include_once "adminsidebar.php";

if (isset($_GET['stid'])) {
        $stid = $_GET['stid'];
        $delstd = $st->deleteStudent($stid);
    }


?>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Showing Student list info</a>
        </li>
      </ol>
		
	<!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> All Students</div>
        <div class="card-body">
          <?php 
            //delete student
              if (isset($delstd)) {
                echo "<h2 style='color:#C90000>".$delstd."</h2><br>";
              }
            
          ?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Sl</th>
                  <th>Name</th>
                  <th>ID</th>
                  <th>DOB</th>
                  <th>Department</th>
                  <th>Photo</th>
                  <th style="width:18%">Action</th>
                </tr>
              </thead>

              <tbody>

                <?php 
                    $i=0;
                    $stlist = $ad->getAllStudent();
                      if ($stlist) {
                        while ($row = $stlist->fetch_assoc()) {
                      $i++;
                 ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo$row['name'];?></td>
                  <td><?php echo$row['st_id'];?></td>
                  <td><?php echo $row['bday'];?></td>
                  <td><?php echo $row['dept'];?></td>
                  <td><img style="width:80px; height:60px;" src="<?php echo $row['image'];?>" ></td>
                  
                  <td><a href="singlestudent.php?stid=<?php echo $row['id'];?>" class="btn btn-sm btn-info">View</a> <a href="editstudent.php?stid=<?php echo $row['id'];?>" class="btn btn-sm btn-primary">Edit</a>  <a class="btn btn-danger btn-sm" href="?stid=<?php echo $row['id'];?>" onclick="return confirm('Are you sure to delete?');">Delete</a><br>
                  <a style="margin-top:4px" href="addcourse.php?stid=<?php echo $row['id'];?>" class="btn btn-sm btn-primary">Add Course grade</a> <a style="margin-top:4px" href="viewcourseresult.php?stid=<?php echo $row['id'];?>" class="btn btn-sm btn-primary">View Result</a>
                  </td>
                </tr>
                
                <?php } }else{

                  echo "<span class='error'>No Student found!</span>";

                } ?>
              </tbody>
             
            </table>
          </div>
        </div>
      </div>
	</div>
    <!-- /.container-fluid-->

<?php
include_once "adminfooter.php";
?>