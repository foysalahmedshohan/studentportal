<?php
include_once "adminheader.php";
include_once "adminsidebar.php";
?>
<div class="container-fluid">
	          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Sl</th>
                  <th>Name</th>
                  <th>ID</th>
                  <th>Department</th>
                  <th>Photo</th>
                  <th style="width:18%">Action</th>
                </tr>
              </thead>

              <tbody>

                <?php 
                    $i=0;
                    $stlist = $ad->getAllStudent();
                      if ($stlist) {
                        while ($row = $stlist->fetch_assoc()) {
                      $i++;
                 ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo$row['name'];?></td>
                  <td><?php echo$row['st_id'];?></td>
                  <td><?php echo $row['dept'];?></td>
                  <td><img style="width:80px; height:60px;" src="<?php echo $row['image'];?>" ></td>
				<td>
                  <a style="margin-top:4px" href="viewregistercourse.php?stid=<?php echo $row['id'];?>" class="btn btn-sm btn-primary">View Pre-registered Courses</a>
                  </td>
                </tr>
                
                <?php } }else{

                  echo "<span class='error'>No Student found!</span>";

                } ?>
              </tbody>
             
            </table>
          </div>
</div>
<?php 
include_once "adminfooter.php";
 ?>
