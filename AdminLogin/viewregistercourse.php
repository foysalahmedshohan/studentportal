<?php
include_once "adminheader.php";
include_once "adminsidebar.php";

if (isset($_GET['stid'])) {
        $stid = $_GET['stid'];
    }

?>
<div class="container-fluid">

    <h2>Pre-registered Course</h2>    
      <div class="list-group" style="color:#000;">
        <table class="table">
          <?php
            $getstudent = $st->getSingleStudent($stid);
            $row = $getstudent->fetch_assoc();
          ?>
          <tr><td>Name:</td> <td><?php echo $row['name']; ?></td></tr>
          <tr><td>ID:</td> <td><?php echo $row['st_id']; ?></td></tr>
          <tr><td>Deparment: </td> <td><?php echo $row['dept']; ?></td></tr>
        </table>
      </div>      
      <table class="table table-bordered" id="MainTable">
        <thead>
          <tr>
            <th>Sl</th>
            <th>Semester</th>
            <th>Course Code</th>
            <th>Course Title</th>
            <th>Credit</th>
            <th>Section</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $getCourse = $st->getRegisteredCourse($stid);
        if ($getCourse) {
          $i=0;
          while ($row = $getCourse->fetch_assoc()) {
            $i++;

      ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $row['semester'];?></td>
            <td><?php echo $row['course_code'];?></td>
            <td><?php echo $row['course_title'];?></td>
            <td><?php echo $row['credit'];?></td>
            <td><?php echo $row['section'];?></td>
          </tr>
          <?php
          }
        }else{
          echo "Not found any course !";
        }
          ?>
        </tbody>
      </table>
</div>
<?php 
include_once "adminfooter.php";
 ?>
