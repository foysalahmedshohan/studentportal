<?php
ob_start();
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache"); 
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
  header("Cache-Control: max-age=2592000");
?>
<?php 
include "libs/Session.php";
  Session::init();
  Session::checkSession();

$filepath = realpath(dirname(__FILE__));
  include_once ($filepath."/helpers/Format.php");
  include_once ($filepath."/libs/Database.php");

  spl_autoload_register(function($class){
    include_once "classes/".$class.".php";
  });

  //creating object of classes
  $db = new Database();
  $fm = new Format();
  $ad = new Admin();
  $st = new Student();
?>


<!DOCTYPE html>
<html>
<head>
<title>DIU</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="AdminLogin/vendor/bootstrap/css/bootstrap.min.css">
<style>
  #MainTable td{
    color: #000;
}
</style>
</head>
<body id="top">

<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 

    <div class="fl_left">
      <ul class="nospace">
        <li><i class="fa fa-phone"></i> +8801666666666</li>
        <li><i class="fa fa-envelope-o"></i> DIU000@gmail.com</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul class="nospace">
        <li><a href="#"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="changepassword.php">Change Password</a></li>
         <?php 
             if(isset($_GET['action']) && $_GET['action']=="logout"){
                      Session::destroy();
                      header("Location: userLogin.php");
                  }
          ?>
        <li><a href="?action=logout">Logout</a></li>
      </ul>
    </div>
  </div>
</div>

<!-- Top Background Image Wrapper -->
<div class="bgded overlay" style="background-image:url('images/10.jpg');"> 
  <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      <div id="logo" class="fl_left">
        <h1><a href="index.html">DIU</a></h1>
        <p>Student Portal</p>
      </div>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a class="drop" href="#">Student</a>
            <ul>
              <li><a href="profile.php">Profile</a></li>
              <li><a href="profileUpdate.php">Update Profile</a></li>
              <li><a href="result.php">Course Result</a></li>
              <li><a href="viewstudentresult.php">Semester Result</a></li>
              
            </ul>
          </li>
         
     
      <li><a class="drop" href="#">Courses</a>
            <ul>
              <li><a href="registerCourses.php">Registerd Courses</a></li>
              <li><a href="preReg.php">Pre-Registration</a></li>
              <li><a href="#">Drop Semester</a></li>
             
              
            </ul>
          </li>
     
          </li>
      
        <li><a class="drop" href="#">Payment</a>
            <ul>
              <li><a href="#">Payment Ledger</a></li>
              <li><a href="#">Payment Sceme</a></li>
             
            </ul>
          </li>
      
        <li><a class="drop" href="#">Certificate</a>
            <ul>
              <li><a href="#">Transcript</a></li>
              <li><a href="#">Certificate</a></li>
             
            </ul>
          </li>
      
      
        <li class=" "><a href="">Teaching Evaluation</a>
           
          </li> 
      
        
        </ul>
      </nav>

    </header>
  </div>