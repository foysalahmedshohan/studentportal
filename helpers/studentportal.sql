-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2018 at 07:12 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_result`
--

CREATE TABLE `tbl_course_result` (
  `id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `course_code` varchar(100) NOT NULL,
  `course_title` varchar(100) NOT NULL,
  `credit` int(10) NOT NULL,
  `grade` varchar(100) NOT NULL,
  `gradepoint` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_result`
--

INSERT INTO `tbl_course_result` (`id`, `st_id`, `course_code`, `course_title`, `credit`, `grade`, `gradepoint`) VALUES
(1, 9, 'CSC - 303', 'Advance Composition', 3, '2', 'B'),
(2, 9, 'CSC - 303', 'Operating System', 4, '2.50', 'B'),
(3, 9, 'csc 391', 'Operating System', 4, '3.50', 'A'),
(4, 10, 'CSC - 461', 'Advance Composition', 3, '3.50', 'A'),
(5, 11, 'CSE303', 'operating System', 3, 'A+', '4.00'),
(6, 11, 'MAT111', 'Math 1', 3, 'A-', '3.50'),
(7, 11, 'ACT303', 'Accounting', 2, 'A', '3.75');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pre_course`
--

CREATE TABLE `tbl_pre_course` (
  `id` int(100) NOT NULL,
  `st_id` varchar(100) NOT NULL,
  `course_code` varchar(100) NOT NULL,
  `course_tittle` varchar(100) NOT NULL,
  `Pre-requisite` varchar(100) NOT NULL,
  `credit` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pre_course`
--

INSERT INTO `tbl_pre_course` (`id`, `st_id`, `course_code`, `course_tittle`, `Pre-requisite`, `credit`) VALUES
(5, '005', 'MAT121', 'Mathematics - II: Linear Algebra and Coordinate Geometry\r\n', 'MAT111\r\n', 3),
(6, '006', 'CSE122', 'Programming and Problem Solving\r\n', 'CSE 112\r\n', 3),
(7, '008', 'CSE123', 'Problem Solving Lab\r\n', 'CSE 112\r\n', 1),
(8, '008', 'PHY123', 'Physics - II: Electricity: Magnetism and Modern Physics\r\n', 'PHY113\r\n', 3),
(9, '009', 'PHY124', 'Physics - II Lab\r\n', 'PHY113\r\n', 1),
(10, '010', 'ENG123', 'Writing and Comprehension\r\n', 'ENG113', 3),
(11, '011', 'CSE132', 'Electrical Circuits', 'PHY123', 3),
(12, '012', 'CSE134', 'Data Structures', 'CSE122', 3),
(13, '013', 'CSE214', 'Object Oriented Programming', 'CSE134', 3),
(14, '014', 'CSE221', 'Algorithm', 'CSE122', 3),
(15, '015', 'CSE231', 'Microprocessor and Assembly Language', 'CSE212', 3),
(16, '016', 'CSE133', 'Electrical Circuits Lab', 'PHY124', 1),
(17, '017', 'CSE135', 'Data Structures Lab', 'CSE123', 1),
(18, '018', 'MAT211', 'Mathematics-IV: Engineering Mathematics', 'MAT131', 3),
(19, '019', 'CSE212', 'Digital Electronics', 'CSE131', 3),
(21, '021', 'CSE213', 'Digital Electronics Lab', 'CSE131', 1),
(22, '022', 'CSE215', 'Object Oriented Programming Lab', 'CSE135', 1),
(23, '023', 'GED201\r\n', 'Bangladesh Studies', 'ENG113', 3),
(24, '024', 'CSE222', 'Algorithm Lab', 'CSE123', 1),
(25, '025', 'STA133', 'Statistics and Probability', 'MAT111', 3),
(26, '026', 'CSE224', 'Electronic Devices and Circuits', 'CSE132', 3),
(27, '027', 'CSE225', 'Electronic Devices and Circuits Lab', 'CSE133', 1),
(28, '028', 'CSE232', 'Microprocessor and Assembly Language Lab', 'CSE133', 1),
(29, '029', 'CSE233', 'Data Communication', 'CSE212', 3),
(30, '030', 'CSE234', 'Numerical Methods', 'MAT211\r\n', 3),
(31, '031', 'CSE322', 'Computer Architecture and Organization', 'CSE232', 3),
(32, '032', 'CSE235', 'Introduction to Bio-Informatics', 'CSE222\r\n', 3),
(33, '033', 'CSE311', 'Database Management System', 'CSE214\r\n', 3),
(34, '034', 'CSE312', 'Database Management System Lab', 'CSE215\r\n', 1),
(35, '035', 'CSE313', 'Computer Network', 'CSE233', 3),
(36, '036', 'CSE314', 'Computer Network Lab', 'CSE233', 1),
(37, '037', 'CSE321', 'System Analysis and Design', 'CSE311', 3),
(38, '038', 'CSE322', 'Computer Architecture and Organization', 'CSE232', 3),
(39, '039', 'CSE323', 'Operating System', 'CSE231', 3),
(40, '040', 'CSE324', 'Operating System Lab', 'CSE231', 1),
(41, '041', 'CSE331', 'Compiler Design', 'CSE222', 3),
(50, '051', 'CSE332', 'Compiler Design lab', 'CSE213', 1),
(52, '052', 'CSE333', 'Software Engineering', 'CSE222', 3),
(53, '053', 'CSE334', 'Wireless Programming', 'CSE233', 3),
(54, '054', 'CSE412', 'Artificial Intelligence', 'CSE213', 3),
(55, '055', 'CSE413', 'Artificial Intelligence Lab', 'CSE213', 1),
(58, '058', 'CSE414', 'Simulation and Modeling', 'CSE213', 3),
(59, '059', 'CSE415', 'Simulation and Modeling Lab', 'CSE213', 1),
(60, '060', 'CSE417', 'Web Engineering', 'CSE222', 3),
(61, '061', 'CSE418', 'Web Engineering Lab', 'CSE222', 1),
(62, '062', 'CSE421', 'Computer Graphics', 'CSE222\r\n', 3),
(63, '063', 'CSE422', 'Computer Graphics Lab', 'CSE222L\r\n', 1),
(64, '064', 'CSE423', 'Embedded System', 'CSE232', 3),
(65, '065', 'CSE450', 'Data Mining', 'CSE311', 3),
(66, '066', 'CSE433', 'Digital Image Processing', 'CSE133', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register_course`
--

CREATE TABLE `tbl_register_course` (
  `id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `course_code` varchar(100) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `credit` int(11) NOT NULL,
  `section` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_register_course`
--

INSERT INTO `tbl_register_course` (`id`, `st_id`, `semester`, `course_code`, `course_title`, `credit`, `section`) VALUES
(1, 9, 'spring 2018', 'CSC - 461', 'Programming Structures', 3, 'A'),
(2, 9, 'spring 2018', 'CSC - 303', 'Operating System', 3, 'B'),
(3, 9, 'spring 2018', 'csc 391', 'Data Structure', 4, 'C'),
(6, 10, 'spring 2018', 'CSC - 303', 'Operating System', 4, 'A'),
(7, 10, 'spring 2018', 'Eng 202', 'Advance Composition', 4, 'B'),
(12, 9, 'summer 2018', 'CSE112', 'Computer Fundamental', 3, 'k'),
(13, 11, 'spring 2018', 'CSE132', 'Electrical Circuits', 3, 'B'),
(14, 11, 'spring 2018', 'CSE134', 'Data Structures', 3, 'B'),
(15, 11, 'spring 2018', 'ACT303', 'Accounting', 2, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sgpa_cgpa`
--

CREATE TABLE `tbl_sgpa_cgpa` (
  `id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `sgpa` varchar(20) NOT NULL,
  `cgpa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sgpa_cgpa`
--

INSERT INTO `tbl_sgpa_cgpa` (`id`, `st_id`, `semester`, `sgpa`, `cgpa`) VALUES
(2, 11, 'spring 2018', '3.69', '3.25'),
(3, 12, 'spring 2018', '3.75', '3.25'),
(5, 12, 'summer 2018', '3.80', '3.36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `bday` date NOT NULL,
  `dept` varchar(100) NOT NULL,
  `admission_date` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `st_id`, `name`, `password`, `blood_group`, `bday`, `dept`, `admission_date`, `email`, `mobile`, `gender`, `address`, `image`) VALUES
(9, 15103058, 'Sanjida', '123', 'A+', '2018-03-22', 'CSE', '2018-03-21', 'Sanjida@gmail.com', '0184546', 'Male', 'Dhaka 1230', 'uploads/7a9f73f30c.jpg'),
(11, 142153679, 'Puja', 'puja', 'A-', '1995-02-01', 'CSE', '2014-03-14', 'puja@diu.edu.bd', '01675449337', 'Female', 'Mohakhali, Dhaka', 'uploads/454cb50e80.png'),
(12, 152155918, 'Puja DIU', 'puja', 'O-', '1999-01-01', 'EEE', '2015-02-01', 'pujadiu@gmail.com', '01523654789', 'Female', 'Shukrabad, Dhaka', 'uploads/c13ff80beb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `email`, `password`) VALUES
(1, 'puja', 'puja@gmail.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_course_result`
--
ALTER TABLE `tbl_course_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pre_course`
--
ALTER TABLE `tbl_pre_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_register_course`
--
ALTER TABLE `tbl_register_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sgpa_cgpa`
--
ALTER TABLE `tbl_sgpa_cgpa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_course_result`
--
ALTER TABLE `tbl_course_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_pre_course`
--
ALTER TABLE `tbl_pre_course`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `tbl_register_course`
--
ALTER TABLE `tbl_register_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_sgpa_cgpa`
--
ALTER TABLE `tbl_sgpa_cgpa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
