<?php
include_once "header.php";
    $stid = Session::get('userid');
?>

<div class="container">

  <h2 style="color:#fff">Update Student Profile</h2>
  <a href="profile.php" class="btn btn-primary">Back to Profile</a>

  <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $added = $st->updateProfile($_POST,$_FILES,$stid);
            if ($added) {
              echo "<h3 style='color:#C90000;text-align:center;margin:10px;'> $added </h3>";
            }
        }
    ?>
        <form action="" method="post" enctype="multipart/form-data">
            <?php
                $getstd = $st->getSingleStudent($stid); 
                $row = $getstd->fetch_assoc();
            ?>
            <div class="row">
                <div class="col-md-6  addEmployee">
                    <div class="form-group">
                      <label for="empName">Student Name:</label>
                      <input type="text" class="form-control addEmployee" id="empName" placeholder="Enter Student Name" name="name" value="<?php echo $row['name'];?>" required>
                    </div>
                    <div class="form-group">
                        <label for="bDate">Birth Date:</label>
                        <input type="date" class="form-control addEmployee" id="datepicker"  placeholder="Enter Student Birth Date" name="dob" value="<?php echo $row['bday'];?>" required>
                    </div>
                    <div class="form-group">
                        <label for="eBlodG">Blood Group:</label>
                        <input type="text" class="form-control addEmployee" id="sBlodG" placeholder="Enter Student Blood Group" name="bg" value="<?php echo $row['blood_group'];?>" required>
                    </div>
                       <div class="form-group">
                        <label for="employeeImage">Upload Photo</label>
                        <img src="AdminLogin/<?php echo $row['image'];?>" style="width:220px; height:120px;" alt="">
                        <input type="file" class="form-control-file" id="sImage" name="image" aria-describedby="fileHelp">
                      </div>
                </div>

            <div class="col-md-6 addEmployee">

            <div class="form-group">
                <label for="positionName">Address:</label>
                <input type="text" class="form-control addEmployee" id="address" placeholder="Enter Address" name="address" value="<?php echo $row['address'];?>" required>
            </div>
            <div class="form-group">
                <label for="eEmail">Email:</label>
                <input type="email" class="form-control addEmployee" id="sEmail" placeholder="Enter Student Mail" name="email" value="<?php echo $row['email'];?>" required>
            </div>
      	  
      	    <div class="form-group">
                <label for="eMobile">Mobile:</label>
                <input type="tel" class="form-control addEmployee" id="sMobile" placeholder="Enter Student Mobile" name="mobile" value="<?php echo $row['mobile'];?>" required>
            </div>

				<hr>
				 <div class="text-center">
                    <div class="form-inline">
                        <button type="submit" class="btn btn-success">Update Profile</button>
                    </div>
                 </div>
				<hr>
            </div>
        </form>
</div>

<?php

include_once "footer.php";
?>