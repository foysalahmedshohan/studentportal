<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../libs/Session.php");
include_once ($filepath."/../libs/Database.php");
include_once ($filepath."/../helpers/Format.php");
	

Class Admin{

	private $db;
	private $fm;
	public function __construct(){
		$this->db = new Database();
		$this->fm = new Format(); 		
	}
								
	//customer login
	public function adminLogin($data){
		$uname = $data['uname'];
		$pass = $data['pass'];
		$uname = $this->fm->validation($data['uname']);
		$uname = mysqli_real_escape_string($this->db->link, $uname);
		$pass = $this->fm->validation($data['pass']);
	    $pass = mysqli_real_escape_string($this->db->link, $pass);
	    $pass = md5($pass);

	    //check empty
	    if (empty($uname) or empty($pass))
		{
			$msg = "Fields must not be empty !";
			return $msg;
		}else{
			//admin login access
			$sql = "SELECT * FROM tbl_user WHERE email='$uname' AND password='$pass'";
			$result = $this->db->select($sql);
			if ($result != false) {
				$value = $result->fetch_assoc();
				Session::set("adminLogin",true);
				Session::set("username",$uname);
				Session::set("userid",$value['id']);
				Session::set("name",$value['name']);
				header("Location: index.php");
			}else{
				$msg = "Username or password not matched !";
				return $msg;
			}
		}
	}


	public function getAdmin(){
		$sql = "SELECT * FROM tbl_user WHERE id=1 ";
		$result = $this->db->select($sql);
		return $result;
	}


	//get All Student list
	public function getAllStudent(){
		
		$sql = "SELECT * FROM tbl_student ORDER BY id DESC ";

		$result = $this->db->select($sql);
		if ($result) {
			return $result;
		}else{
			return false;
		}
	}

	//Add student profile
	public function insertStudent($data, $file){
		//validation of student data
		$st_id = $this->fm->validation($data['st_id']);
		$st_id = mysqli_real_escape_string($this->db->link, $st_id);

		$name = $this->fm->validation($data['name']);
		$name = mysqli_real_escape_string($this->db->link, $name);


		$password = $data['password'];
		$password = mysqli_real_escape_string($this->db->link, $password);

		$gender = $data['gender'];
		$gender = mysqli_real_escape_string($this->db->link, $gender);

		$bg = $data['bg'];
		$bg = mysqli_real_escape_string($this->db->link, $bg);

		$dob = $data['dob'];
		//list($month, $day, $year) = explode("/",$dob);
		//$dob = "$year-$month-$day";

		$dept = $data['dept'];
		$dept = mysqli_real_escape_string($this->db->link, $dept);
		$address = $data['address'];
		$address = mysqli_real_escape_string($this->db->link, $address);

		$email = $data['email'];
		$email = mysqli_real_escape_string($this->db->link, $email);

		$mobile = $data['mobile'];
		$mobile = mysqli_real_escape_string($this->db->link, $mobile);

		$joindate = $this->fm->validation($data['joindate']);
		$joindate = mysqli_real_escape_string($this->db->link, $joindate);


		//take image information using super global variable $_FILES[];
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_name = $file['image']['name'];
		$file_size = $file['image']['size'];
		$file_temp = $file['image']['tmp_name'];

		
		if (empty($st_id) or empty($name) or empty($gender) or empty($dob) or empty($dept))
		{
			$msg = "<span class='error'>Fields must not be empty !.</span>";
			return $msg;
		}else{
			//validate uploaded images
			$div = explode('.', $file_name);
			$file_ext = strtolower(end($div));
			$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
			$uploaded_image = "uploads/".$unique_image;
			
			if ($file_size >10048567) {
				$msg = "<span class='error'>Student not found !.</span>";
				return $msg;
			} elseif (in_array($file_ext, $permited) === false) {
				echo "<span class='error'>You can upload only:-"
				.implode(', ', $permited)."</span>";
			}else{
				move_uploaded_file($file_temp, $uploaded_image);
				
				$query = "INSERT INTO tbl_student(st_id, name, password,blood_group,bday,dept,admission_date,email,mobile,gender,address,image) 
				VALUES('$st_id','$name','$password','$bg','$dob','$dept','$joindate','$email','$mobile','$gender','$address','$uploaded_image')";

				$inserted = $this->db->insert($query);
				if ($inserted) {
					$msg = "<span class='success'>Student profile inserted successfully.</span>";
					return $msg;
				}else{
					$msg = "<span class='error'>Failed to insert.</span>";
					return $msg;
				}
		 	}

		}

	}


	//register course
	public function courseResult($data, $id){

		$course_code = $data['course_code'];
		$course_code = mysqli_real_escape_string($this->db->link, $course_code);

		$course_title = $this->fm->validation($data['course_title']);
		$course_title = mysqli_real_escape_string($this->db->link, $course_title);

		$credit = $this->fm->validation($data['credit']);
		$credit = mysqli_real_escape_string($this->db->link, $credit);

		$grade = $this->fm->validation($data['grade']);
		$grade = mysqli_real_escape_string($this->db->link, $grade);

		$gradepoint = $this->fm->validation($data['gradepoint']);
		$gradepoint = mysqli_real_escape_string($this->db->link, $gradepoint);
		
		if (empty($course_code) or empty($course_title) or empty($credit) or empty($credit) or empty($gradepoint))
		{
			$msg = "<span class='error'>Fields must not be empty !.</span>";
			return $msg;
		}else{	
			$query = "INSERT INTO tbl_course_result(st_id,course_code,course_title,credit,grade, gradepoint) 
			VALUES('$id','$course_code','$course_title','$credit','$grade','$gradepoint')";

			$inserted = $this->db->insert($query);
			if ($inserted) {
				$msg = "<span class='success'>Course result submited successfully.</span>";
				return $msg;
			}else{
				$msg = "<span class='error'>Failed to submit.</span>";
				return $msg;
			}
	 	  }
		}


//end of Admin class
	}
?>