<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../libs/Session.php");
include_once ($filepath."/../libs/Database.php");
include_once ($filepath."/../helpers/Format.php");

Class Student{

	private $db;
	private $fm;
	public function __construct(){
		$this->db = new Database();
		$this->fm = new Format(); 		
	}

	//student login
	//customer login
	public function studentLogin($data){
		$st_id = $data['st_id'];
		$password = $data['password'];
		$st_id = $this->fm->validation($data['st_id']);
		$st_id = mysqli_real_escape_string($this->db->link, $st_id);
		$password = $this->fm->validation($data['password']);
	    $password = mysqli_real_escape_string($this->db->link, $password);

	    //check empty
	    if (empty($st_id) or empty($password))
		{
			$msg = "Fields must not be empty !";
			return $msg;
		}else{
			//admin login access
			$sql = "SELECT * FROM tbl_student WHERE st_id='$st_id' AND password='$password'";
			$result = $this->db->select($sql);
			if ($result != false) {
				$value = $result->fetch_assoc();
				Session::set("login",true);
				Session::set("st_id",$st_id);
				Session::set("userid",$value['id']);
				Session::set("name",$value['name']);
				header("Location: index.php");
			}else{
				$msg = "Username or password not matched !";
				return $msg;
			}
		}
	}

	//change pasword

	public function changePassword($data, $id){
		$oldpass  = $data['currentpass'];

		$npass    = $data['newpassword'];

		$sql = "select * from tbl_student where id='$id'";
		$result = $this->db->select($sql);
		
		while ($old = $result->fetch_assoc()) {
			if($old['password'] == $oldpass){

				if (!empty($npass)) {
					$sql  = "update tbl_student set password='$npass' where id='$id' ";
					$result = $this->db->update($sql);
					if ($result) {
						return true;
					
					}else{
						return false;
						}
				}
			}
				
		}
		
	}



	//get single student by ID
	public function getSingleStudent($id){
		$pid = $this->fm->validation($id);
		$pid = mysqli_real_escape_string($this->db->link, $pid);
		$sql = "SELECT * FROM tbl_student WHERE id = '$pid' ";
		$result = $this->db->select($sql);
		return $result;
	}

	//delete product by product id
	public function deleteStudent($id){
		$getImgSql =  "SELECT * FROM tbl_student WHERE id='$id'";
		$getimg = $this->db->select($getImgSql);
		if ($getimg) {
			while ($rows = $getimg->fetch_assoc()) {
				$delimg = $rows['image'];
				unlink($delimg);
			}
		}
		//query for delete all info from product table	
		$sql = "DELETE FROM tbl_student WHERE id='$id'";
		$result = $this->db->delete($sql);
		if ($result) {
			$msg = "<span class='success'>Student profile Successfully Deleted !.</span>";
			return $msg;
		}else{
			$msg = "<span class='error'>Failed to Delete.</span>";
			return $msg;
		}
	}

	//update product by product id
	public function updateStudent($data, $file, $id){
		
		$name = $this->fm->validation($data['name']);
		$name = mysqli_real_escape_string($this->db->link, $name);
		
		$bg = $data['bg'];
		$bg = mysqli_real_escape_string($this->db->link, $bg);

		$dob = $data['dob'];
		//list($month, $day, $year) = explode("/",$dob);
		//$dob = "$year-$month-$day";

		$dept = $data['dept'];
		$dept = mysqli_real_escape_string($this->db->link, $dept);
		$address = $data['address'];
		$address = mysqli_real_escape_string($this->db->link, $address);

		$email = $data['email'];
		$email = mysqli_real_escape_string($this->db->link, $email);

		$mobile = $data['mobile'];
		$mobile = mysqli_real_escape_string($this->db->link, $mobile);

		$joindate = $this->fm->validation($data['joindate']);
		$joindate = mysqli_real_escape_string($this->db->link, $joindate);



		//take image information using super global variable $_FILES[];
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_name = $file['image']['name'];
		$file_size = $file['image']['size'];
		$file_temp = $file['image']['tmp_name'];

		
		if (empty($name) or empty($dob) or empty($joindate) or empty($dept))
		{
			$msg = "<span class='error'>Fields must not be empty !.</span>";
			return $msg;
		}else{

			if (!empty($file_name)) {
				//delete previous image
				$getImgSql =  "SELECT * FROM tbl_student WHERE id='$id'";
				$getimg = $this->db->select($getImgSql);
				if ($getimg) {
					while ($rows = $getimg->fetch_assoc()) {
						$delimg = $rows['image'];
						unlink($delimg);
					}
				}

				//validate uploaded images
				$div = explode('.', $file_name);
				$file_ext = strtolower(end($div));
				$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
				$uploaded_image = "uploads/".$unique_image;
				
				if ($file_size >10048567) {
					$msg = "<span class='error'>Student not found !.</span>";
					return $msg;
				} elseif (in_array($file_ext, $permited) === false) {
					echo "<span class='error'>You can upload only:-"
					.implode(', ', $permited)."</span>";
				}else{
					move_uploaded_file($file_temp, $uploaded_image);
					$query = "UPDATE tbl_student
							  SET 
							  name     = '$name',
							  blood_group     = '$bg',
							  bday            = '$dob',
							  dept            = '$dept',
							  admission_date  = '$joindate',
							  email 		  = '$email',
							  mobile 		  = '$mobile',
							  address 		  = '$address',
							  image           = '$uploaded_image'
							  WHERE id        = '$id' ";

					$updated = $this->db->update($query);
					if ($updated) {
						$msg = "<span class='success'>Student profile updated successfully.</span>";
						return $msg;
					}else{
						$msg = "<span class='error'>Failed to updated.</span>";
						return $msg;
					}
			 	}
			}else{

				    $query = "UPDATE tbl_student
							  SET
							  name   	      = '$name',
							  blood_group     = '$bg',
							  bday            = '$dob',
							  dept            = '$dept',
							  admission_date  = '$joindate',
							  email 		  = '$email',
							  mobile 		  = '$mobile',
							  address 		  = '$address'
							  WHERE id        = '$id' ";


					$updated = $this->db->update($query);
					if ($updated) {
						$msg = "<span class='success'>Student profile updated successfully.</span>";
						return $msg;
					}else{
						$msg = "<span class='error'>Failed to updated.</span>";
						return $msg;
					}

			}
			
		}

	}

	//update student profile
	//update product by product id
	public function updateProfile($data, $file, $id){
		
		$name = $this->fm->validation($data['name']);
		$name = mysqli_real_escape_string($this->db->link, $name);
		
		$bg = $data['bg'];
		$bg = mysqli_real_escape_string($this->db->link, $bg);

		$dob = $data['dob'];

		$address = $data['address'];
		$address = mysqli_real_escape_string($this->db->link, $address);

		$email = $data['email'];
		$email = mysqli_real_escape_string($this->db->link, $email);

		$mobile = $data['mobile'];
		$mobile = mysqli_real_escape_string($this->db->link, $mobile);



		//take image information using super global variable $_FILES[];
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_name = $file['image']['name'];
		$file_size = $file['image']['size'];
		$file_temp = $file['image']['tmp_name'];

		
		if (empty($name) or empty($dob))
		{
			$msg = "<span class='error'>Fields must not be empty !.</span>";
			return $msg;
		}else{

			if (!empty($file_name)) {
				//delete previous image
				$getImgSql =  "SELECT * FROM tbl_student WHERE id='$id'";
				$getimg = $this->db->select($getImgSql);
				if ($getimg) {
					while ($rows = $getimg->fetch_assoc()) {
						$delimg = "AdminLogin/".$rows['image'];
						unlink($delimg);
					}
				}

				//validate uploaded images
				$div = explode('.', $file_name);
				$file_ext = strtolower(end($div));
				$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
				$uploaded_image = "uploads/".$unique_image;
				$mv_up_file = "AdminLogin/uploads/".$unique_image;
				
				if ($file_size >10048567) {
					$msg = "<span class='error'>Student not found !.</span>";
					return $msg;
				} elseif (in_array($file_ext, $permited) === false) {
					echo "<span class='error'>You can upload only:-"
					.implode(', ', $permited)."</span>";
				}else{
					move_uploaded_file($file_temp, $mv_up_file);
					$query = "UPDATE tbl_student
							  SET 
							  name     = '$name',
							  blood_group     = '$bg',
							  bday            = '$dob',
							  email 		  = '$email',
							  mobile 		  = '$mobile',
							  address 		  = '$address',
							  image           = '$uploaded_image'
							  WHERE id        = '$id' ";

					$updated = $this->db->update($query);
					if ($updated) {
						$msg = "<span class='success'>Student profile updated successfully.</span>";
						return $msg;
					}else{
						$msg = "<span class='error'>Failed to updated.</span>";
						return $msg;
					}
			 	}
			}else{

				    $query = "UPDATE tbl_student
							  SET
							  name   	      = '$name',
							  blood_group     = '$bg',
							  bday            = '$dob',
							  email 		  = '$email',
							  mobile 		  = '$mobile',
							  address 		  = '$address'
							  WHERE id        = '$id' ";


					$updated = $this->db->update($query);
					if ($updated) {
						$msg = "<span class='success'>Student profile updated successfully.</span>";
						return $msg;
					}else{
						$msg = "<span class='error'>Failed to updated.</span>";
						return $msg;
					}

			}
			
		}

	}

	//register course
	public function registerCourse($data, $stid){
		$semester = $this->fm->validation($data['semester']);
		$semester = mysqli_real_escape_string($this->db->link, $semester);

		$course_code = $data['course_code'];
		$course_code = mysqli_real_escape_string($this->db->link, $course_code);

		$course_title = $this->fm->validation($data['course_title']);
		$course_title = mysqli_real_escape_string($this->db->link, $course_title);

		$credit = $this->fm->validation($data['credit']);
		$credit = mysqli_real_escape_string($this->db->link, $credit);

		$section = $this->fm->validation($data['section']);
		$section = mysqli_real_escape_string($this->db->link, $section);
		
		if (empty($semester) or empty($course_code) or empty($course_title) or empty($credit) or empty($section))
		{
			$msg = "<span class='error'>Fields must not be empty !.</span>";
			return $msg;
		}else{	

			$sqlcourse = "SELECT * FROM tbl_register_course WHERE course_code = '$course_code' AND st_id='$stid' ";
			$getcourse = $this->db->select($sqlcourse);
			if ($getcourse) {
				$msg = "You have already taken this course !";
				return $msg;
			}else{
				$upCourse = strtoupper($course_code);
				$sql_pre = "SELECT * FROM tbl_pre_course WHERE course_code='$upCourse' ";
				$pre = $this->db->select($sql_pre);
				
				//var_dump($pre);
				if ($pre) {
					$prerequisite = $pre->fetch_assoc();
					$msg = "You have to do this prerequisite(".$prerequisite['Pre-requisite'].") first";
					return $msg;
				}else{
					$sql = "SELECT SUM(credit) AS total_credit FROM tbl_register_course WHERE semester = '$semester' AND st_id='$stid'";
					$result = $this->db->select($sql);
					$total = $result->fetch_assoc();
					if ($total['total_credit'] > 15) {
						return $msg = "You can not take more than 15 credits";
					}else{
						$query = "INSERT INTO tbl_register_course(st_id,semester,course_code,course_title,credit,section) 
						VALUES('$stid','$semester','$course_code','$course_title','$credit','$section')";

						$inserted = $this->db->insert($query);
						if ($inserted) {
							$msg = "<span class='success'>Course registered successfully.</span>";
							return $msg;
						}else{
							$msg = "<span class='error'>Failed to regiser.</span>";
							return $msg;
						}
				 	  }
				 	}
		 	     }
		     }
		}

	public function getRegisteredCourse($stid){
		
		$sql = "SELECT * FROM tbl_register_course WHERE st_id='$stid' ORDER BY id DESC";
	
		$result = $this->db->select($sql);
		if ($result) {
			return $result;
		}else{
			//$msg = "<span class='error'>No Posts found !.</span>";
			return false;
		}
	}

	public function getCourseResult($stid){
		$sql = "SELECT * FROM tbl_course_result WHERE st_id='$stid' ORDER BY id DESC";
		$result = $this->db->select($sql);
		return $result;
	}


	public function addSgpaAndCgpa($data, $stid)
	{
		$semester = $this->fm->validation($data['semester']);
		$semester = mysqli_real_escape_string($this->db->link, $semester);

		$sgpa = $data['sgpa'];
		$sgpa = mysqli_real_escape_string($this->db->link, $sgpa);

		$cgpa = $this->fm->validation($data['cgpa']);
		$cgpa = mysqli_real_escape_string($this->db->link, $cgpa);

		$query = "INSERT INTO tbl_sgpa_cgpa(st_id,semester,sgpa,cgpa) 
						VALUES('$stid','$semester','$sgpa','$cgpa')";

		$inserted = $this->db->insert($query);
		if ($inserted) {
			$msg = "<span class='success'>Semester result successfully.</span>";
			return $msg;
		}else{
			$msg = "<span class='error'>Failed to add result.</span>";
			return $msg;
		}
	}
	//get sgpa and cgpa result

	public function getSgpaCgpa($stid)
	{
		$sql = "SELECT * FROM tbl_sgpa_cgpa WHERE st_id='$stid' ORDER BY id DESC";
		$result = $this->db->select($sql);
		return $result;
	}



//end of class
}

?>