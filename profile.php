<?php
include_once "header.php";
?>
<?php 
        $stid = Session::get('userid');

        $getstd = $st->getSingleStudent($stid);
       	$row = $getstd->fetch_assoc();
?>
 <div class="container">

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			 <table class="table table-striped" style="color:black; font-weight: bold; background-color:lightgray;">
			    <tbody>
					<tr>
	<td><img  style="width300px; height:180px; border-radius:100px" src="<?php echo "AdminLogin/".$row['image'];?>" alt=""></td>
					</tr>
			        <tr>
						<td>Name</td>
						<td><?php echo $row['name'];?></td>
			         </tr>
			        <tr>
			        	<td>Studen ID:</td>
			        	<td><?php echo $row['st_id'];?></td>
			        </tr>
			        <tr>
			        	<td>Date of birth:</td>
			         	<td><?php echo $row['bday'];?></td>
			         </tr>
			        <tr>
			        	<td>Blood Group:</td>
			         	 <td><?php echo $row['blood_group'];?></td>
			        </tr>
			        <tr>
			        	<td>Department:</td>
			        	<td><?php echo $row['dept'];?></td>
			        </tr>
			        <tr>
			        	<td>Mobile:</td>
			        	<td><?php echo $row['mobile'];?></td>
			        </tr>
			        <tr>
			        	<td>Email:</td>
			        	<td><?php echo $row['email'];?></td>
			        </tr>
			        <tr>
			        	<td>Gender:</td>
			        	<td><?php echo $row['gender'];?></td>
			        </tr>
			        <tr>
			        	<td>Admission Date:</td>
			        	<td><?php echo $row['admission_date'];?></td>
			         </tr>
			        <tr>
			        	<td>Address:</td>
			            <td><?php echo $row['address'];?></td>
			        </tr>
			      <tr>
			      	<a href="profileupdate.php" class="btn btn-primary">Edit Profile</a>
			      </tr>
			    </tbody>
			  </table>
		 </div>
		  <div class="col-md-2"></div>
	</div>
</div>
	
<?php

include_once "footer.php";
?>