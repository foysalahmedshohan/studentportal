<?php
include "header.php";
?>

<div class="container">
	<h2>Registered Course</h2>     
	<hr>      
  <table class="table table-bordered" id="MainTable">
    <thead>
      <tr>
        <th>Sl</th>
        <th>Semester</th>
        <th>Course Code</th>
        <th>Course Title</th>
        <th>Credit</th>
        <th>Section</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $stid = Session::get("userid");
		$getCourse = $st->getRegisteredCourse($stid);
		if ($getCourse) {
			$i=0;
			while ($row = $getCourse->fetch_assoc()) {
				$i++;

	?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row['semester'];?></td>
        <td><?php echo $row['course_code'];?></td>
        <td><?php echo $row['course_title'];?></td>
        <td><?php echo $row['credit'];?></td>
        <td><?php echo $row['section'];?></td>
      </tr>
      <?php
			}
		}else{
			echo "Not found any course !";
		}
      ?>
    </tbody>
  </table>
</div>

<?php

include "footer.php";
?>