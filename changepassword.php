<?php 
  include_once "header.php";
 ?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="form">
				<?php 
					if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				        $added = $st->changePassword($_POST, Session::get('userid'));
				        if ($added) {
				        	echo "<h3><span class='success' style='text-align:center;margin:10px;'>Password successfully changed !</span></h3>";
				        }else{
				        	echo "<h3><span class='error' style='text-align:center;margin:10px;'>Your current password not matched	!</span></h3>";
				        }
				    }

				 ?>
				<form action="" method="POST">
				
				  <div class="form-group">
				    <label for="CurentPassword">Current password:</label>
				    <input type="password" class="form-control" name="currentpass" id="CurentPassword" placeholder="Current Password">
				  </div>
				
				  <div class="form-group">
				    <label for="NewPassword">Password:</label>
				    <input type="password" class="form-control" name="newpassword" id="NewPassword" placeholder="New Password">
				  </div>
				
				  <button type="submit" class="btn btn-primary">change password</button>
				
				</form>
			</div>
		</div>
	</div>
</div>

<?php 
  include_once "footer.php";
 ?>
