<?php
include "header.php";
?>
<div class="container addEmployee">
<?php
    $stid  = Session::get("userid");

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $added = $st->registerCourse($_POST, $stid);
            if ($added) {
              echo "<h3 class='success' style='color:#fff;text-align:center;margin:10px;'>$added</h3>";
            }
        }
    ?>
  <h2>Add Course Details</h2>
      <div class="help-block" ><span syle="background-color:#C82333;">One semester maximum 15 credit</span></div>
        <form action="" method="post">
            <div class="row">
                <div class="col-md-6  addEmployee">
                  <div class="form-group">
                  <label for="semester">Select Semester:</label>
                  <select name="semester" class="form-control" id="" style="background:#66CCCC">
                    <option value="spring 2018">Spring 2018</option>
                    <option value="summer 2018">Summer 2018</option>
                    <option value="fall 2018">Fall 2018</option>
                    <option value="spring 2018">Spring 2019</option>
                    <option value="summer 2018">Summer 2019</option>
                    <option value="fall 2018">Fall 2019</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="empName">Course Code:</label>
                  <input type="text" class="form-control addEmployee" id="ccode" placeholder="Enter Course Code" name="course_code" required>
                </div>

                   <div class="form-group">
                      <label for="empName">Course Tittle:</label>
                      <input type="text" class="form-control addEmployee" id="ctittle" placeholder="Enter Course Tittle" name="course_title" required>
                    </div>
             
                </div>

            <div class="col-md-6  addEmployee">
              
              <div class="form-group">
                    <label for="eNid">Credit:</label>
                    <input type="text" class="form-control addEmployee" id="credit" placeholder="Enter Course Credit" name="credit" required>
                </div>
              <div class="form-group">
                  <label for="eSection">Section:</label>
                  <input type="text" class="form-control addEmployee" id="grade" placeholder="Enter section Name" name="section" required>
              </div>

            </div>
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </form>
</div>

<?php

include "footer.php";
?>