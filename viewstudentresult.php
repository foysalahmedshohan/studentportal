<?php
include "header.php";
?>
<div class="container addEmployee">
<?php
    $stid  = Session::get("userid");
?>
  <h2>Semester Result</h2>    
        <div class="list-group" style="color:#000;">
            <table class="table">
              <?php
                $getstudent = $st->getSingleStudent($stid);
                $row = $getstudent->fetch_assoc();
              ?>
              <tr><td>Name:</td> <td><?php echo $row['name']; ?></td></tr>
              <tr><td>ID:</td> <td><?php echo $row['st_id']; ?></td></tr>
              <tr><td>Deparment: </td> <td><?php echo $row['dept']; ?></td></tr>
            </table>
        </div>  
    
      <table class="table table-bordered" id="MainTable">
        <thead>
          <tr>
            <th>Sl</th>
            <th>Semester</th>
            <th>Sgpa</th>
            <th>Cgpa</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $getCourse = $st->getSgpaCgpa($stid);
        if ($getCourse) {
          $i=0;
          while ($row = $getCourse->fetch_assoc()) {
            $i++;

      ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $row['semester'];?></td>
            <td><?php echo $row['sgpa'];?></td>
            <td><?php echo $row['cgpa'];?></td>
          </tr>
          <?php
          }
        }else{
          echo "Not found any course !";
        }
          ?>
        </tbody>
      </table>
</div>

<?php

include "footer.php";
?>