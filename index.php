<?php 
  include_once "header.php";
 ?>

  <div id="pageintro" class="hoc clear"> 

    <article>
      <p class="heading">Focus your carrear</p>
      <h2 class="heading">Daffodil international university</h2>
      <p>A dintiguished landmark of higher university</p>
      <footer>
        <ul class="nospace inline pushright">
          <li><a class="btn" href="#">Gallery</a></li>
          <li><a class="btn inverse" href="#">Comment</a></li>
        </ul>
      </footer>
    </article>
  </div>

</div>

<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <article class="one_third first">
      <p class="nospace font-xs">Dictum vitae molestie</p>
      <h4 class="font-x2 font-x3">Scelerisque imperdiet sed</h4>
      <p>Porta ut consequat sit amet est morbi sodales neque a nibh congue bibendum nunc eget malesuada massa nam porta.</p>
      <p class="btmspace-30">Purus in eros pellentesque non pellentesque purus elementum mauris et efficitur quam nullam turpis purus</p>
      <footer><a class="btn inverse" href="#">Read More &raquo;</a></footer>
    </article>
    <article class="one_third"><a href="#"><img class="btmspace-30" src="images/demo/320x220.png" alt=""></a>
      <h4 class="heading">Ultrices vivamus</h4>
      <p>Magna quis justo elementum porttitor donec malesuada aliquam efficitur nam nec augue maximus consectetur&hellip;</p>
      <p class="nospace"><a href="#">Read More &raquo;</a></p>
    </article>
    <article class="one_third"><a href="#"><img class="btmspace-30" src="images/demo/320x220.png" alt=""></a>
      <h4 class="heading">Tincidunt hendrerit</h4>
      <p>Lacus non commodo felis donec quis metus in mi facilisis lacinia ut sed velit sed eget tellus sed quam mollis&hellip;</p>
      <p class="nospace"><a href="#">Read More &raquo;</a></p>
    </article>
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>







<?php 
  include_once "footer.php";
 ?>
